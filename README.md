## Тестовое задание Perx

## Условия https://gist.github.com/softzilla/a75e80b1ea7acf128a95dac0b2209b52

## Демо https://fizikst.github.io/perx/index.html

## Установка

* Установить модули

```sh
npm i
```

* Запустить проект

```sh
npm start
```

* Сборка проект

```sh
npm run build
```