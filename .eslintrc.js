module.exports = {
  parser: 'babel-eslint',
  extends: [
    'airbnb',
  ],    
  env: {
    browser: true,
  },
  settings: {
    'import/resolver': 'webpack',
  },  
  rules:  {
    // Custom
    'import/no-unresolved': 'off',
    'react/forbid-prop-types': 'off',
    'react/jsx-filename-extension': ['error', { 'extensions': ['.js', '.jsx'] }],
  }
}
