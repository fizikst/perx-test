import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import UsersList from 'pages/users/UsersList';

import { types, actions } from 'modules/users';

class Users extends React.Component {
  constructor(props) {
    super(props);
    props.loadData();
  }

  render() {
    const { requests, users } = this.props;
    const request = requests[types.REQUEST_LOAD_USERS];

    return (
      <Fragment>
        {request && (
          <Fragment>
            {request.loading && (
            <div>
              Loading...
            </div>
            )}
            {request.error && request.errorMessage}            
          </Fragment>
        )}
        {users.length > 0 && <UsersList rows={users} />}
      </Fragment>
    );
  }
}

Users.propTypes = {
  loadData: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
  users: state.users,
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(actions.fetchLoadUsers());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Users);
