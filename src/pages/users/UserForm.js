import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { types, actions } from 'modules/users';
import { combineRequests } from 'modules/requests';

class UserForm extends React.PureComponent {
  render() {
    const {
      invalid, pristine, submitting, handleSubmit, requests,
    } = this.props;

    const request = combineRequests([
      requests[types.REQUEST_CREATE_USER],
      requests[types.REQUEST_UPDATE_USER],
    ]);
    return (
      <Fragment>
        {request && (
          <Fragment>
            {request.loading && (
            <div>
              Loading...
            </div>
            )}
            {request.error && request.errorMessage}
            {request.success && 'Success!'}
          </Fragment>
        )}
        <form onSubmit={handleSubmit}>
          <Field name="objectId" component="input" type="hidden" />
          <Field name="name" component="input" type="text" placeholder="Name" />
          <Field name="email" component="input" type="text" placeholder="Email" />
          <Field name="password" component="input" type="text" placeholder="Password" />
          <button type="submit" disabled={invalid || pristine || submitting}>
            Отправить
          </button>
        </form>
      </Fragment>
    );
  }
}

const onSubmit = (formData, dispatch) => {
  if (formData.objectId) {
    dispatch(actions.fetchUpdateUser(formData.objectId, formData));
  } else {
    dispatch(actions.fetchCreateUser(formData));
  }
};

UserForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  requests: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
});

export default connect(
  mapStateToProps,
)(
  reduxForm({
    onSubmit,
    form: 'USER_FORM',
  })(UserForm),
);
