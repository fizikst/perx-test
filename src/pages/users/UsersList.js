import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import { actions } from 'modules/users';
import { cleanUp } from 'modules/requests';
import Popup from './Popup';

class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      row: {},
    };
  }

  handleEdit = (e, row) => {
    const { cleanCache } = this.props;
    this.setState({ open: true, row });
    cleanCache();
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleDelete = (e, row) => {
    const { deleteUser } = this.props;
    deleteUser(row.objectId);
  }

  render() {
    const { rows } = this.props;
    const { open, row } = this.state;
    return (
      <Fragment>
        <Button variant="contained" onClick={e => this.handleEdit(e, {})}>
          Add
        </Button>
        <Popup open={open} row={row} onClose={this.handleClose} />
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Name
                </TableCell>
                <TableCell>
                  Email
                </TableCell>
                <TableCell>
                  Password
                </TableCell>
                <TableCell>
                  Operations
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(r => (
                <TableRow key={r.objectId}>
                  <TableCell>
                    {r.name}
                  </TableCell>
                  <TableCell>
                    {r.email}
                  </TableCell>
                  <TableCell>
                    {r.password}
                  </TableCell>
                  <TableCell>
                    <Button onClick={e => this.handleEdit(e, r)}>
                      Edit
                    </Button>
                    <Button
                      onClick={(e) => {
                        if (
                          window.confirm(
                            'Delete this item?',
                          )
                        ) {
                          this.handleDelete(e, r);
                        }
                      }}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </Fragment>
    );
  }
}

UsersList.propTypes = {
  rows: PropTypes.array.isRequired,
  deleteUser: PropTypes.func.isRequired,
  cleanCache: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  deleteUser(objectId) {
    dispatch(actions.fetchDeleteUser(objectId));
  },
  cleanCache() {
    dispatch(cleanUp());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersList);
