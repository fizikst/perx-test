import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import UserForm from './UserForm';

class Popup extends React.PureComponent {
  handleClose = () => {
    const { onClose } = this.props;
    onClose();
  };

  render() {
    const { open, row } = this.props;
    return (
      <Dialog onClose={this.handleClose} open={open}>
        <DialogContent>
          <UserForm initialValues={row} />
        </DialogContent>
      </Dialog>
    );
  }
}

Popup.propTypes = {
  open: PropTypes.bool.isRequired,
  row: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Popup;
