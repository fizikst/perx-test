import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Users from 'pages/Users';

const Routes = () => (
  <Router>
    <Switch>
      <Route path="/" component={Users} />
    </Switch>
  </Router>
);

export default Routes;
