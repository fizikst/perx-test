import { SUCCESS } from 'modules/requests';
import { types, actions } from 'modules/users';

export default store => next => (action) => {
  if (action.type === SUCCESS) {
    if (action.meta.request === types.REQUEST_LOAD_USERS) {
      store.dispatch({
        type: types.SET_USERS,
        payload: action.payload,
      });
    }

    if (
      action.meta.request === types.REQUEST_CREATE_USER
      || action.meta.request === types.REQUEST_UPDATE_USER
      || action.meta.request === types.REQUEST_DELETE_USER
    ) {
      store.dispatch(actions.fetchLoadUsers());
    }
  }

  return next(action);
};
