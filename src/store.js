import { createStore, combineReducers, applyMiddleware } from 'redux';

import usersReducer from 'modules/users';
import requestsReducer from 'modules/requests';

import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import requests from 'utils/middleware/requests';
import { createLogger } from 'redux-logger';
import { apiMiddleware } from 'redux-api-middleware';

const middleware = [apiMiddleware, requests, thunk];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger({ collapsed: true }));
}

const rootReducer = combineReducers({
  form: formReducer,
  users: usersReducer,
  requests: requestsReducer,
});

const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
