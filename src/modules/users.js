import { RSAA } from 'redux-api-middleware';
import { REQUEST, SUCCESS, FAILURE } from 'modules/requests';

const host = process.env.BACKEND_URL;

export const types = {
  SET_USERS: 'USERS/SET_USERS',
  REQUEST_LOAD_USERS: 'USERS/REQUEST_LOAD_USERS',
  REQUEST_CREATE_USER: 'USERS/REQUEST_CREATE_USER',
  REQUEST_UPDATE_USER: 'USERS/REQUEST_UPDATE_USER',
  REQUEST_DELETE_USER: 'USERS/REQUEST_DELETE_USER',
};

// Reducer
const reducer = (state = [], action) => {
  switch (action.type) {
    case types.SET_USERS:
      return action.payload;
    default:
      return state;
  }
};

// Actions
const fetchLoadUsers = () => ({
  [RSAA]: {
    endpoint: `${host}/data/Users`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_LOAD_USERS,
      },
    })),
  },
});

const fetchCreateUser = formData => ({
  [RSAA]: {
    endpoint: `${host}/data/Users`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_CREATE_USER,
      },
    })),
  },
});

const fetchUpdateUser = (objectId, formData) => ({
  [RSAA]: {
    endpoint: `${host}/data/Users/${objectId}`,
    method: 'PUT',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_UPDATE_USER,
      },
    })),
  },
});

const fetchDeleteUser = objectId => ({
  [RSAA]: {
    endpoint: `${host}/data/Users/${objectId}`,
    method: 'DELETE',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_DELETE_USER,
      },
    })),
  },
});

export const actions = {
  fetchLoadUsers,
  fetchCreateUser,
  fetchUpdateUser,
  fetchDeleteUser,
};

export default reducer;
