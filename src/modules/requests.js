export const REQUEST = 'REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const CLEAN_UP = 'CLEAN_UP';

const states = {
  error: false,
  success: false,
  loading: false,
  errorMessage: '',
};

export const combineRequests = requestsList => Object.keys({ ...states })
  .map(
    v => requestsList.find(r => r && r[v])
        && requestsList.find(r => r && r[v])[v],
  )
  .reduce(
    (accumulator, item, index) => {
      const result = accumulator;
      result[Object.keys(accumulator)[index]] = item;
      return result;
    },
    { ...states },
  );

const entity = (state = { ...states }, action) => {
  switch (action.type) {
    case REQUEST:
      return {
        ...state,
        error: false,
        success: false,
        loading: true,
      };
    case SUCCESS:
      return {
        ...state,
        error: false,
        success: true,
        loading: false,
        data: action.payload,
      };
    case FAILURE:
      return {
        ...state,
        error: true,
        success: false,
        loading: false,
        errorMessage: action.payload.response.message,
      };
    default:
      return state;
  }
};

export const cleanUp = () => ({
  type: CLEAN_UP,
});

export default (state = {}, action) => {
  switch (action.type) {
    case REQUEST:
    case SUCCESS:
    case FAILURE:
      return {
        ...state,
        [action.meta.request]: entity(state[action.meta], action),
      };
    case CLEAN_UP:
      return {};
    default:
      return state;
  }
};
