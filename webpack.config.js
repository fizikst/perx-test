const os = require('os');
const path = require('path');
const webpack = require('webpack');

const productionEnv = process.env.NODE_ENV === "production";

const config = {
  backend_url: 'http://api.backendless.com/53FB6874-BEE0-9546-FFCA-1F3DEE56BE00/73E38E38-C5C9-45B2-FFB5-D05A6E16A600',
};

const APP_PATH = path.join(os.homedir(), __dirname);
const PUBLIC_PATH = path.join(APP_PATH, 'public');
let entry = {
  app: "./index.js",
};

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      BACKEND_URL:
        JSON.stringify(process.env.BACKEND_URL) ||
        JSON.stringify(config.backend_url),
    },
  })
];

module.exports = {
  mode: 'development',
  
  context: path.resolve(APP_PATH, "src" ),

  devtool: productionEnv ? "source-map" : "cheap-module-source-map",

  entry,

  output: {
    filename: '[name].bundle.js',
    publicPath: '/',
    path: PUBLIC_PATH,
    pathinfo: true,
  },
  resolve: {
    modules: [
      path.resolve( "./src" ),
      "node_modules",
    ],
    alias: {
      root: APP_PATH,
      components: path.resolve(APP_PATH, "src/components" ),
      modules: path.resolve(APP_PATH, "src/modules" ),
      utils: path.resolve(APP_PATH, "src/utils" ),
      pages: path.resolve(APP_PATH, "src/pages" ),
    }
  },
  module: {      
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ]
  },
  devServer: {
    historyApiFallback: true,
    contentBase: PUBLIC_PATH,
  },
  plugins,
};
